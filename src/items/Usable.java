package items;

/** In contrast to Consumable, this Item is not destroyed after use.
 *
 */
public interface Usable {
}

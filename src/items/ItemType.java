package items;

/** ItemType is a enum which list all kind of Item.
 *
 * This enum is useful only with Item.get_rand_item, to randomize the kind of Item.
 */
public enum ItemType {
    ARMOR,
    BOOTS,
    FOOD,
    GAUNTLETS,
    HELMET,
    MUNITION,
    POTION,
    RING,
    SHIELD,
    WEAPON_CONTACT,
    WEAPON_DISTANCE
}

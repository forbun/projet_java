package items;

import character.BodyPart;

import java.util.ArrayList;

/** This item can be worn by an Entity.
 *
 */
public abstract class Wearable extends Item {
    private ArrayList<BodyPart> _allowedPart;

    public Wearable(String name, String desc) {
        super(name, desc);

        _allowedPart = new ArrayList<BodyPart>();
    }

    public boolean addAllowedPart(BodyPart part) {
        return _allowedPart.add(part);
    }

    public ArrayList<BodyPart> getAllowedParts() {
        return _allowedPart;
    }

    public boolean removeAllowedPart(BodyPart part) {
        return _allowedPart.remove(part);
    }
}

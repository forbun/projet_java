package items;

/**
 * Created by root on 23/10/15.
 */
public class DoorKey extends Item {
    public DoorKey() {
        super("Door key", "A simple door key... You know which door it opens ?");
    }
}

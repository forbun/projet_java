package items;

/** This item can be used only once, it is destroyed after.
 *
 */
public interface Consumable {
}

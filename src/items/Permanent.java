package items;

/** This Item can't be dropped, its effects are permanent.
 *
 */
public interface Permanent {
}

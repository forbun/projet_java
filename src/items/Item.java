package items;

import items.type.*;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Random;

/** The common class of each Item.
 *
 */
public abstract class Item implements Serializable {
    //Fields
    private HashMap<Affected, Integer> _effects;
    private final String _name;
    private final String _desc;
    private static Random _randItem = new Random();

    /** The constructor, with minimal information.
     *
     */
    public Item(String name, String desc) {
        _effects = new HashMap<Affected, Integer>();
        _name = name;
        _desc = desc;
    }

    /** Add an effect, with the couple (effect, value).
     *
     *  @return the previous value for the effect
     */
    public Integer addEffect(Affected aff, Integer value) {
        return _effects.put(aff, value);
    }

    /** Get an effect, with the couple (effect, value).
     *
     */
    public Integer getEffect(Affected aff) {
        return _effects.get(aff);
    }

    /** Remove an effect.
     *
     *  @return the previous value for the effect
     */
    public Integer removeEffect(Affected aff) {
        return _effects.remove(aff);
    }

    /** Get the name of the Item.
     *
     */
    public String getName() {
        return _name;
    }

    /** Get the description of the Item.
     *
     */
    public String getDescription() {
        return _desc;
    }

    /** Get the description of all effects for this Item.
     *
     */
    public String getEffects() {
        String s = "";

        if(_effects.isEmpty()) {
            s += " No effects.\n";
        } else {
            for (Affected aff : _effects.keySet()) {
                s += " " + aff + " : " + _effects.get(aff) + "\n";
            }
        }

        return s;
    }

    @Override
    public String toString() {
        String s = "";

        s += getName() + " :\n";
        s += " " + getDescription() + "\n";
        s += getEffects();

        return s;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof Item) {
            Item item = (Item) o;

            // Classes are the same
            if(!this.getClass().getName().equals(item.getClass().getName())) {
                return false;
            }
            // Names are the same
            if(!this._name.equals(item._name)) {
                return false;
            }
            // Desc are the same
            if(!this._desc.equals(item._desc)) {
                return false;
            }
            // Effects are the same kind
            if(!this._effects.equals(item._effects)) {
                return false;
            }

            return true;
        }

        return false;
    }

    /** Get the next random number.
     *
     */
    public static int item_rng() {
        return Math.abs(_randItem.nextInt());
    }

    /** Create a random Item.
     *
     */
    public static Item get_rand_item(int level) {
        Item res = null;

        // Set the type
        ItemType typeObj = ItemType.values()[item_rng() % ItemType.values().length];
        switch (typeObj) {
            case ARMOR:
                res = new Armor("Armor_" + item_rng(), "");
                break;
            case BOOTS:
                res = new Boots("Boots_" + item_rng(), "");
                break;
            case FOOD:
                res = new Food("Food_" + item_rng(), "");
                break;
            case GAUNTLETS:
                res = new Gauntlets("Gauntlets_" + item_rng(), "");
                break;
            case HELMET:
                res = new Helmet("Helmet_" + item_rng(), "");
                break;
            case MUNITION:
                res = new Munition("Munition_" + item_rng(), "");
                break;
            case POTION:
                res = new Potion("Potion_" + item_rng(), "");
                break;
            case RING:
                res = new Ring("Ring_" + item_rng(), "");
                break;
            case SHIELD:
                res = new Shield("Shield_" + item_rng(), "");
                break;
            case WEAPON_CONTACT:
                res = new ContactWeapon("Weapon_Contact_" + item_rng(), "");
                break;
            case WEAPON_DISTANCE:
                res = new DistanceWeapon("Weapon_Distance_" + item_rng(), "");
                break;
        }

        // Add all effects
        int chance = 15;
        for(Affected aff : Affected.values()) {
            if(item_rng() % 100 < chance) {
                res.addEffect(aff, item_rng() % 50);
            }
        }

        return res;
    }
}

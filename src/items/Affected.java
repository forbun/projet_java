package items;

/** List all possible affected characteristics.
 *
 */
public enum Affected {
    LIFE("Life"),
    MANA("Mana"),
    STRENGTH("Strength"),
    DEXTERITY("Dexterity"),
    INTELLIGENCE("Intelligence"),
    MAX_LIFE("Max Life"),
    MAX_MANA("Max Mana");

    private String _name;

    Affected(String name) {
        _name = name;
    }

    @Override
    public String toString() {
        return _name;
    }
}

package items.type;

import character.BodyPart;
import items.Item;
import items.Wearable;

import java.io.Serializable;

/**
 * .
 *
 * @author bunlang
 */
public class Boots extends Wearable implements Serializable {
    public Boots(String name, String desc) {
        super(name, desc);

        addAllowedPart(BodyPart.LEGS);
    }
}

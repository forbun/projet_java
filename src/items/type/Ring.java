package items.type;

import character.BodyPart;
import items.Item;
import items.Wearable;

import java.io.Serializable;

/**
 * .
 *
 * @author bunlang
 */
public class Ring extends Wearable implements Serializable {
    public Ring(String name, String desc) {
        super(name, desc);

        addAllowedPart(BodyPart.FINGER);
    }
}

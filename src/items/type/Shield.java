package items.type;

import character.BodyPart;
import items.Item;
import items.Wearable;

import java.io.Serializable;

/**
 * .
 *
 * @author bunlang
 */
public class Shield extends Wearable implements Serializable {
    public Shield(String name, String desc) {
        super(name, desc);

        addAllowedPart(BodyPart.HAND_RIGHT);
        addAllowedPart(BodyPart.HAND_LEFT);
    }
}

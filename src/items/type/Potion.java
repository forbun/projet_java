package items.type;

import items.Consumable;
import items.Item;

import java.io.Serializable;

/**
 * .
 *
 * @author bunlang
 */
public class Potion extends Item implements Consumable, Serializable {
    public Potion(String name, String desc) {
        super(name, desc);
    }
}

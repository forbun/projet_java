package items.type;

import items.Consumable;
import items.Item;

import java.io.Serializable;

/**
 * .
 *
 * @author bunlang
 */
public class Food extends Item implements Consumable, Serializable {
    public Food(String name, String desc) {
        super(name, desc);
    }
}

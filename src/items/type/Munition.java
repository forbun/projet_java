package items.type;

import items.Consumable;
import items.Item;

import java.io.Serializable;

/**
 * .
 *
 * @author bunlang
 */
public class Munition extends Item implements Consumable, Serializable {
    public Munition(String name, String desc) {
        super(name, desc);
    }
}

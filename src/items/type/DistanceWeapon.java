package items.type;

import java.io.Serializable;

/**
 * .
 *
 * @author bunlang
 */
public class DistanceWeapon extends Weapon implements Serializable {
    public DistanceWeapon(String name, String desc) {
        super(name, desc);
    }
}

package items.type;

import character.BodyPart;
import items.Item;
import items.Wearable;

import java.io.Serializable;

/**
 * .
 *
 * @author bunlang
 */
public class Helmet extends Wearable implements Serializable {
    public Helmet(String name, String desc) {
        super(name, desc);

        addAllowedPart(BodyPart.HEAD);
    }
}

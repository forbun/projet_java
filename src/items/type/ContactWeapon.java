package items.type;

import java.io.Serializable;

/**
 * .
 *
 * @author bunlang
 */
public class ContactWeapon extends Weapon implements Serializable {
    public ContactWeapon(String name, String desc) {
        super(name, desc);
    }
}

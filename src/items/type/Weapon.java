package items.type;

import character.BodyPart;
import items.Item;
import items.Usable;
import items.Wearable;

import java.io.Serializable;

/**
 * .
 *
 * @author bunlang
 */
public class Weapon extends Wearable implements Usable, Serializable {
    public Weapon(String name, String desc) {
        super(name, desc);

        addAllowedPart(BodyPart.HAND_RIGHT);
        addAllowedPart(BodyPart.HAND_LEFT);
    }
}

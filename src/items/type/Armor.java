package items.type;

import character.BodyPart;
import items.Item;
import items.Wearable;

import java.io.Serializable;

/**
 * .
 *
 * @author bunlang
 */
public class Armor extends Wearable implements Serializable {
    public Armor(String name, String desc) {
        super(name, desc);

        addAllowedPart(BodyPart.CHEST);
    }
}

package items.type;

import character.BodyPart;
import items.Item;
import items.Wearable;

import java.io.Serializable;

/**
 * .
 *
 * @author bunlang
 */
public class Gauntlets extends Wearable implements Serializable {
    public Gauntlets(String name, String desc) {
        super(name, desc);

        addAllowedPart(BodyPart.ARMS);
    }
}

import character.BodyPart;
import character.Player;
import character.Professions;
import character.Races;
import character.exceptions.UnconsumableException;
import dungeon_building.Door;
import dungeon_building.Dungeon;
import dungeon_building.Room;
import dungeon_building.Save;
import dungeon_building.exceptions.DoorException;
import items.Consumable;
import items.Item;
import items.Wearable;

import java.util.ArrayList;
import java.util.Scanner;

/** This is the core of the Game.
 *
 */
public class Game {
    Dungeon _dungeon;
    int _currLevel;
    Room _currRoom;
    ArrayList<Player> _players;
    boolean _continue;
    Scanner _sc;

    /** The default constructor : create a new Game.
     *
     */
    public Game() {
        _sc = new Scanner(System.in);
        _dungeon = null;
        _currLevel = -1;
        _currRoom = null;
        _players = new ArrayList<Player>();
        _continue = true;
    }

    /** Another constructor : load a Game with the Save mysave.
     *
     */
    public Game(Save mysave) {
        _sc = new Scanner(System.in);
        _dungeon = null;
        _currLevel = -1;
        _currRoom = null;
        _players = new ArrayList<Player>();
        _continue = true;

        _dungeon = new Dungeon(mysave);
        addPlayer(mysave.get_player());

        System.out.println("  Saved game loaded.");
    }

    /** Build a new Dungeon.
     *
     * You need to use this function only when you create a new Game.
     */
    public void buildDungeon(int diff, int seed) {
        _dungeon = new Dungeon(diff, seed);
    }

    /** Build a new Player.
     *
     * You need to use this function only when you create a new Game.
     */
    public void buildPlayer() {
        // Intro
        System.out.println("  Welcome in The Tower Of Ptarmigan Moor !");
        System.out.println("  Before starting, you must create your avatar.");

        // Name
        String name;
        System.out.println("  First, what's your name ?");
        System.out.print("> ");
        name = _sc.nextLine();

        // Race
        Races race = null;
        String valRace;
        System.out.println("  Now, choose a Race :");
        while(race == null) {
            for (Races r : Races.values()) {
                System.out.println("  - " + r.name());
            }
            System.out.print("> ");
            valRace = _sc.nextLine();
            try {
                // Can throw some exceptions
                race = Races.valueOf(valRace);
            } catch (IllegalArgumentException e) {
                System.out.println("  Can't recognize " + valRace + " race.");
                System.out.println("  TIP : Races name are upper-cased...");
            } catch (NullPointerException e) {
                System.out.println("  Error with your choice.");
            }
        }

        // Profession
        Professions profession = null;
        String valProf;
        System.out.println("  Finally, choose a Profession :");
        while(profession == null) {
            for (Professions p : Professions.values()) {
                System.out.println("  - " + p.name());
            }
            System.out.print("> ");
            valProf = _sc.nextLine();
            try {
                // Can throw some exceptions
                profession = Professions.valueOf(valProf);
            } catch (IllegalArgumentException e) {
                System.out.println("  Can't recognize " + valProf + " profession.");
                System.out.println("  TIP : Profession name are upper-cased...");
            } catch (NullPointerException e) {
                System.out.println("  Error with your choice.");
            }
        }

        // Creation !
        Player p = new Player(name, race, profession, 1);
        addPlayer(p);
        System.out.println("  Player created !");
    }

    /** Add a new player in the Game.
     */
    public void addPlayer(Player p) {
        _players.add(p);
    }

    /** Let's go !
     *
     */
    public void startGame() {

        for(Player p : _players) {
            p.goToStart(_dungeon);
            _currRoom = p.getLocation();
        }

        System.out.println("  Press Enter to start the game.");
        _sc.nextLine();

        makeAChoice();
    }

    /** This is the main function : it waits an input from the player.
     *
     */
    private void makeAChoice() {
        while(_continue) {

            System.out.println("  What do you want to do ?");

            System.out.print("> ");
            String input = _sc.nextLine();
            String inputArr[] = input.split(" ");

            // Find the command
            Commands[] cmds = Commands.values();
            int i = 0;
            while(i < cmds.length &&                                   // Not OutOfBounds
                    (!inputArr[0].equals(cmds[i].getCommand()) ||      // Not the same command
                      inputArr.length != cmds[i].getNbParameters()+1 ) // Not the good number of params
                 ) {
                i++;
            }

            // Find what stopped the while loop
            if(i >= cmds.length) {
                System.out.println("  Cannot recognize this command. Type \"help\".");
            } else {
                applyChoice(cmds[i], inputArr);
            }
        }

        System.out.println("Goodbye my friend...");
    }

    ///////////////////////////////////////////////////////////////////////////
    //                              All choices                              //
    ///////////////////////////////////////////////////////////////////////////

    /** Choose the good action* function with the Commands value cmd.
     *
     */
    private void applyChoice(Commands cmd, String[] argc) {
        switch(cmd) {
            //////////////////////////////////////////////////////////////////////////
            case EQUIP:
                actionEquip(argc[1]);
                break;
            //////////////////////////////////////////////////////////////////////////
            case GO:
                actionGo(argc[1]);
                break;
            //////////////////////////////////////////////////////////////////////////
            case HELP:
                actionHelp();
                break;
            //////////////////////////////////////////////////////////////////////////
            case INSPECT:
                actionInspect();
                break;
            //////////////////////////////////////////////////////////////////////////
            case LOOK_BAG:
                actionLookBag();
                break;
            //////////////////////////////////////////////////////////////////////////
            case LOOK_ITEM:
                actionLookItem(argc[1]);
                break;
            //////////////////////////////////////////////////////////////////////////
            case LOOK_ME:
                actionLookMe();
                break;
            //////////////////////////////////////////////////////////////////////////
            case LOOK_ROOM:
                actionLookRoom();
                break;
            //////////////////////////////////////////////////////////////////////////
            case QUIT:
                _continue = false;
                break;
            //////////////////////////////////////////////////////////////////////////
            case SAVE:
                actionSave(argc[1]);
                break;
            //////////////////////////////////////////////////////////////////////////
            case TAKE:
                // Not used yet.
                break;
            //////////////////////////////////////////////////////////////////////////
            case UNEQUIP:
                actionUnequip(argc[1]);
                break;
            //////////////////////////////////////////////////////////////////////////
            case USE:
                actionUse(argc[1]);
                break;
            //////////////////////////////////////////////////////////////////////////
            case USE_ON:
                // Not used yet.
                break;
            //////////////////////////////////////////////////////////////////////////
        }

    }

    ///////////////////////////////////////////////////////////////////////////
    //                              Each choice                              //
    ///////////////////////////////////////////////////////////////////////////

    private void actionEquip(String name) {
        Item item = _players.get(0).getItemByName(name);
        if(item == null) {
            System.out.println(name + " doesn't exist in your bag.");
        } else if(item instanceof Wearable) {
            _players.get(0).equip((Wearable) item);
            System.out.println(name + " equipped.");
        } else {
            System.out.println("Can't equip " + name);
        }
    }

    private void actionGo(String nm) {
        Integer idDoor = Integer.decode(nm);
        ArrayList<Door> doors = _currRoom.get_visible_doors();

        if(idDoor >= doors.size()) {
            System.out.println("  There is no door with this number !");

            return;
        }

        // Check if this door is a death door...
        if(doors.get(idDoor).is_death) {
            System.out.println("  You hear a voice which say : \"Are you sure to open this door ?\"");
            System.out.print("> [y/n] : ");

            Scanner sc = new Scanner(System.in);
            char c = '\0';

            while(c != 'y' && c != 'n') {
                c = sc.next().charAt(0);
            }

            if(c == 'n') {
                return;
            }
        }

        for(Player p : _players) {
            try {
                p.open_door(doors.get(idDoor), _currRoom);
                if(_currRoom != p.getLocation()) {
                    _currRoom = p.getLocation();
                }
            } catch (DoorException e) {
                e.printStackTrace();
            }
        }
    }

    private void actionHelp() {
        // The help description is stored in each enum
        Commands[] cmdArr = Commands.values();
        for(Commands cmdItem : cmdArr) {
            System.out.println(cmdItem.getHelpText());
        }
        System.out.println("");
    }

    private void actionInspect() {
        int oldNbVisibleDoors = _currRoom.get_nb_visible_doors();

        // Search doors...
        for(Player p : _players) {
            p.search_room(_currRoom);
        }

        int newNbVisibleDoors = _currRoom.get_nb_visible_doors();
        int diffVisibleDoors = newNbVisibleDoors - oldNbVisibleDoors;

        System.out.println("You have discovered " + diffVisibleDoors + " door(s).");
    }

    private void actionLookBag() {
        for(Player p : _players) {
            ArrayList<Item> bag = p.get_bag();
            int i = 1;

            if(bag.size() == 0) {
                System.out.println("No item in your bag.");
            } else {
                for (Item it : bag) {
                    System.out.println(i + "/" + bag.size() + ":");
                    System.out.println(it);
                }
            }
        }
    }

    private void actionLookItem(String name) {
        Item itm = _players.get(0).getItemByName(name);
        if(itm == null) {
            System.out.println("  " + name + "doesn't exist in your bag.");
        } else {
            System.out.println(itm);
        }
    }

    private void actionLookMe() {
        for(Player p : _players) {
            System.out.println(p);
        }
    }

    private void actionLookRoom() {
        System.out.println(_currRoom.get_description());

        if(_currRoom.get_nb_portes() - _currRoom.get_nb_visible_doors() > 0) {
            System.out.println("\nYou can't see all the room because of the darkness...");
        }
        System.out.println("\nSo, there is " + _currRoom.get_nb_visible_doors() + " door(s)...\n");
    }

    private void actionSave(String name) {
        Save mysave = new Save(name, _players.get(0));
        mysave.set_name(name);
        mysave.dump();
    }

    private void actionUnequip(String name) {
        try {
            // valueOf() may throw IllegalArgumentException or NullPointerException
            BodyPart bp = BodyPart.valueOf(name);

            _players.get(0).unequip(bp);
            System.out.println("  " + name + " unequipped.");
        } catch (IllegalArgumentException e) {
            System.out.println("  Can't recognize " + name + " body part.");
            System.out.println("  TIP : Body parts are upper-cased...");
            // Print name of all body parts
            for(BodyPart bp : BodyPart.values()) {
                System.out.print("  " + bp.name());
            }
            System.out.println("");
        } catch (NullPointerException e) {
            System.out.println("  NullPointerException in Game...");
        }
    }

    private void actionUse(String name) {
        Item it = _players.get(0).getItemByName(name);
        if(it == null) {
            System.out.println("  " + name + " doesn't exist in your bag.");
        } else {
            if(it instanceof Consumable) {
                try {
                    _players.get(0).consume((Consumable) it);
                    System.out.println("  " + name + " consumed.");
                } catch (UnconsumableException e) {
                    System.out.println(e.getMessage());
                }
            } else {
                System.out.println("  Can't use " + name + ".");
            }
        }
    }
}

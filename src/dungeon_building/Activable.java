package dungeon_building;

import dungeon_building.exceptions.ActivableException;

/**
 * Created by root on 21/10/15.
 */
public interface Activable {

    boolean deactivate() throws ActivableException;
    boolean activate() throws ActivableException;
}

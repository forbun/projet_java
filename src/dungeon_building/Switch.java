package dungeon_building;

import dungeon_building.exceptions.ActivableException;

/**
 * Created by root on 21/10/15.
 */
public class Switch {
    private Activable target;
    public boolean hidden;
    public int hiding_score;

    public Switch(Activable target, boolean hidden, Place mylevel){
        this.target = target;
        this.hidden = hidden;
        this.hiding_score = hidden?mylevel.dungeon.difficulty:0;
    }

    public void switch_on() throws ActivableException {
        this.target.activate();
    }

    public void switch_off()throws ActivableException{
        this.target.deactivate();
    }

    public String reveal_purpose(){
        if(this.target instanceof Trap){
            return "It's a Trap!";
        }
        else{
            return "its a door!";
        }
    }
}

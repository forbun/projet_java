package dungeon_building;

import java.util.ArrayList;
import java.util.Random;
/**
 * Created by root on 20/10/15.
 */
public class Dungeon {
    int difficulty;
    private int seed;
    private final int m =  961748941;
    private final int q= 961749043;
    private static int state;
    private ArrayList<Place> levels;
    private Random randBuild;
    private Random randFight;

    public Dungeon(int diff, int seed){
        this.difficulty = diff;
        this.randBuild = new Random(seed);
        this.randFight = new Random(seed);
        Random rand = new Random();
        this.levels = new ArrayList<Place>();
        this.seed = seed;
        while((this.seed == 0 || this.seed == 1)&&!Dungeon.coPrime(this.seed,this.m*this.q)){
            System.out.println("this is not a valid seed! "+this.seed);
            this.seed = rand.nextInt((Integer.MAX_VALUE - 1) + 1) + 1;
        }

        this.state = 0;
    }

    public Dungeon(Save mysave){
        this.difficulty = mysave.get_level();
        this.randBuild = new Random(seed);
        this.randFight = new Random(seed);
        this.seed = mysave.get_level_seed();
        this.levels = new ArrayList<Place>();
        this.state = 0;
    }

    public int get_seed(){return this.seed;}
    private static int gcd(int a, int b) {
        int t;
        while(b != 0){
            t = a;
            a = b;
            b = t%b;
        }
        return a;
    }

    public int get_state(){
        return this.state;
    }

    private static boolean coPrime(int a, int b) {
        return gcd(a,b) == 1;
    }

    public int build_rng(){
        return Math.abs(randBuild.nextInt());
    }
    public int combat_rng(){
        return Math.abs(randFight.nextInt());
    }
    public Room go_to_level(int level){
        if(level > this.levels.size()){
            level = this.levels.size();
        }
        if(this.levels.size() <= level || this.levels.get(level) == null){
            this.levels.add(new Place(this,level));
        }
        return this.levels.get(level).get_room(0);
    }
}

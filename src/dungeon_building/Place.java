package dungeon_building;

import character.Monster;
import character.Professions;
import character.Races;
import dungeon_building.enumerations.Themes;
import dungeon_building.exceptions.PlaceException;
import items.Item;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by root on 20/10/15.
 */
public class Place {
    private int level_number;
    private int gen_seed;
    private int nb_monstres;
    private Themes style;
    private int difficulty;
    public Dungeon dungeon;
    private ArrayList<Room> rooms;
    private HashMap<Room,ArrayList<Room>> adjacency;
    private int nbrooms;

    public final int MAX_ROOMS = 20;
    public final int MAX_DEPTH = 4;
    public final int MIN_ROOMS = 1;

    private final int MAX_PORTES = 4;

    public int getLevel_number(){
        return this.level_number;
    }

    public Place(Dungeon D,int level_number){
        this.dungeon = D;
        this.gen_seed = D.get_state();
        this.level_number = level_number;
        this.difficulty = D.difficulty == 0?level_number:level_number * D.difficulty;
        this.rooms = new ArrayList<Room>();
        this.adjacency = new HashMap<Room,ArrayList<Room>>();
        this.style = Themes.random_Theme(this.dungeon);
        this.nbrooms = this.dungeon.build_rng() % MAX_ROOMS;
        this.nb_monstres = this.difficulty == 0?this.nbrooms:this.nbrooms * this.difficulty;
        int totalmonstres = this.nb_monstres;

        //now lets add the straight path between entrance and exit:
        for(int i = 0; i < this.MAX_DEPTH;i++) {
            Room newroom = new Room(this);
            this.rooms.add(newroom);
        }

        //now lets link them
        this.build_main_path();

        try {
            this.check_main_path();
        }
        catch(PlaceException ex){
            System.out.println("encountered error : "+ex.getMessage());
        }
        //adds all other rooms
        for(int i = this.rooms.size();i < this.nbrooms;i++){
            this.rooms.add(new Room(this));
        }

        for(int i = this.MAX_DEPTH;i < this.nbrooms;i++){
            Room tmp = this.get_room(i);
            Room from = this.get_rand_room();
            Door newdoor = this.link_rooms(tmp, from);
            this.rooms.add(tmp);
        }

        //and now lets place monsters:
        int max_nb_per_room = this.nb_monstres/2;
        while(this.nb_monstres > 0){
            Room chosen = this.get_rand_room();
            int dice = this.dungeon.build_rng();
            int nb_monstres_to_create;
            if(this.nb_monstres == 1){
                nb_monstres_to_create = 1;
            }
            else if(this.nb_monstres > max_nb_per_room && max_nb_per_room > 1){
                nb_monstres_to_create = dice % max_nb_per_room;
            }
            else{
                nb_monstres_to_create = dice % (this.nb_monstres + 1);
            }
            boolean isfriendly = this.dungeon.build_rng() % 10 == 0;
            this.nb_monstres -= nb_monstres_to_create;
            for(int i = 0; i < nb_monstres_to_create;i++){
                Monster newmonster = new Monster("Monster", Races.random_race(this.dungeon),
                        Professions.random_profession(this.dungeon),this.level_number,isfriendly,chosen);

                chosen.add_monster(newmonster);
            }
        }

        //and now lets make a treasure room:
        Room treasureRoom= this.get_rand_room();
        for(int i = 0; i < totalmonstres;i++){
            treasureRoom.add_to_coffer(Item.get_rand_item(this.level_number));
        }
    }

    public Themes get_style(){
        return this.style;
    }

    public int get_difficulty(){
        return this.difficulty;
    }
    public void build_main_path(){
        for(int i = 0; i < this.rooms.size();i++){
            boolean hasexit = i == this.rooms.size() -1;
            boolean hasentry = i == 0;
            if(hasentry){
                this.link_rooms(null, this.get_room(i));
            }
            else if (hasexit) {
                this.link_rooms(this.get_room(i - 1), this.get_room(i));
                this.link_rooms(this.get_room(i), null);
            }
            else{
                this.link_rooms(this.get_room(i - 1), this.get_room(i));
            }
        }
    }

    public int getGen_seed(){
        return this.gen_seed;
    }

    private Room get_rand_room(){
        int ind = this.dungeon.build_rng() % this.rooms.size();
        return this.rooms.get(ind);
    }

    private Door link_rooms(Room a, Room b){
        boolean hidden = this.dungeon.build_rng() % 4 == 0;//one out of four door is hidden
        boolean remotely_activated = this.dungeon.build_rng() %4 == 0; //on out of foor door is remotely activated
        boolean trapped = this.dungeon.build_rng() % (10-this.difficulty) == 0;
        Door d = new Door(a,b,hidden,null,null,this);
        if(remotely_activated){
            Switch monint = new Switch(d,hidden,this);
            this.get_rand_room().add_interrupteur(monint);
        }
        if(trapped){
            Trap mytrap = new Trap(1 + this.difficulty,1 + this.difficulty);
            d.trap = mytrap;
        }
        if(a != null) {
            a.add_door(d);
            if(hidden){
              a.inc_hidden_door(1);
            }
        }
        else{//this is the first room
            d.is_death = true;
        }
        if(b != null) {
            b.add_door(d);
            if(hidden){
                b.inc_hidden_door(1);
            }
        }

        return d;
    }
    public Room get_room(int index){
        return this.rooms.get(index);
    }

    public Room get_last(){
        return this.get_room(this.rooms.size() - 1);
    }

    public Room get_first(){
        return this.get_room(0);
    }
    public void check_main_path() throws PlaceException {

    }
}

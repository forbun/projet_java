package dungeon_building;

import dungeon_building.enumerations.Room_part;
import dungeon_building.enumerations.Themes;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by root on 27/10/15.
 */
public class Room_Description {
    public String floor;
    public String ceiling;
    public String walls;
    public String doors;
    public String atmosphere;
    private Room room;

    private static HashMap<Themes,HashMap<Room_part,ArrayList<String>>> components = null;

    public Room_Description(Room myroom){
        if (Room_Description.components == null){
            Room_Description.init_components();
        }
        this.room = myroom;
        for(Room_part k : Room_part.values()){
            this.init_me(k);
        }
    }

    private void init_me(Room_part k){
        HashMap<Room_part,ArrayList<String>> mycomp = Room_Description.components.get(this.room.level.get_style());
        ArrayList<String> tmp = mycomp.get(k);
        int ind;

        if(k == Room_part.atmosphere){
            ind = this.room.level.dungeon.build_rng() % tmp.size();
            this.atmosphere = tmp.get(ind);
        }
        else if (k == Room_part.walls){
            ind = this.room.level.dungeon.build_rng() % tmp.size();
            this.walls = tmp.get(ind);
        }
        else if (k == Room_part.ceiling){
            ind = this.room.level.dungeon.build_rng() % tmp.size();
            this.ceiling = tmp.get(ind);
        }
        else if (k == Room_part.floor){
            ind = this.room.level.dungeon.build_rng() % tmp.size();
            this.floor = tmp.get(ind);
        }
        else{
            ind = this.room.level.dungeon.build_rng() % tmp.size();
            this.doors = tmp.get(ind);
        }
    }

    /*
    This functions creates a deep structure made of two hashmaps, the first one associates a theme to a hashmap containings
    for each room_part a list of possible descriptions. When the room description is built it will use the theme to access
    each room part in turn and then randomly select a sub description. All of this using the following model:

    The room has a <atmosphere adjective> atmosphere, as the door you just passed closes you notice it is made of <door material>
    the floor is made of <floor material>, the ceiling is of <ceiling material>. As you gaze upon the walls you see that they are
    made of <ceiling material>
     */

    private static void init_components(){
        Room_Description.components = new HashMap<Themes,HashMap<Room_part,ArrayList<String>>>();
        for(Themes j : Themes.values()){
            HashMap<Room_part,ArrayList<String>> tmpentry = new HashMap<Room_part,ArrayList<String>>();
           for(Room_part k : Room_part.values()){
               ArrayList<String> tmparray = new ArrayList<String>();
               if(j == Themes.Cave || j == Themes.Lava){
                   if (k == Room_part.floor || k == Room_part.ceiling || k == Room_part.walls) {
                       tmparray.add("granite");
                       tmparray.add("schist");
                       tmparray.add("chrysoprase");
                       tmparray.add("hematite");
                       tmparray.add("clay");
                       tmparray.add("sandy clay");
                       tmparray.add("loam");
                       tmparray.add("clay loam");
                       tmparray.add("compacted sand");
                       tmparray.add("ruby");
                       tmparray.add("saphire");
                       tmparray.add("emerald");
                       tmparray.add("strange, lifelike caryatids with eyes of obsidian");
                       tmparray.add("gold ore");
                       tmparray.add("copper ore");
                       tmparray.add("impossibly arranged octagonal tiles");
                       tmparray.add("bones... So many bones you recoil in horror at the slaughter it ought to have taken" +
                               " to build it");
                       tmparray.add("fountains of molten rock with floating tiles of diamond and obsidian");
                   }
                   else if (k == Room_part.atmosphere){
                       tmparray.add("is dry, lifeless");
                       tmparray.add("is insufferably hot");
                       tmparray.add("is so Cold... How could it be?");
                       tmparray.add("reeks of mould and ageless death");
                       tmparray.add("is Normal? that's strange...");
                       tmparray.add("is subterannean, you can almost here the Parcaes bickering about you");
                       tmparray.add("is filled with whispers that you can not completely understand... Something about..." +
                               "peer shaped bananas?");
                   }
                   else{
                       tmparray.add("made of magical engravings in the wall material");
                       tmparray.add("like  teeth in a masterfully carved monster's mouth");
                       tmparray.add("piles of rubble that used to be tunnels");
                   }
               }
               else if(j == Themes.Jungle){
                   if (k == Room_part.floor || k == Room_part.ceiling || k == Room_part.walls){
                       tmparray.add("writhing lianas, covered in a pus like yellowish substance");
                       tmparray.add("the thick trunks and branches of ageless trees");
                       tmparray.add("bristling twigs that seem to whisper and creak on their own accord");
                       tmparray.add("earth held together by monstruously twisted roots");
                       tmparray.add("LIVE SNAKES!!! No, they are dead and dried");
                   }
                   else if(k == Room_part.atmosphere){
                       tmparray.add("is insufferably hot");
                       tmparray.add("is a bit Cold... How could it be?");
                       tmparray.add("reeks of mould and ageless death");
                       tmparray.add("conspicuously normal? that's strange...");
                       tmparray.add("is filled with whispers that you can not completely understand...\n Something about..." +
                               "peer shaped loinclothes?");
                   }
                   else{
                       tmparray.add("old and wood wrought, standing in the middle of a clearing");
                       tmparray.add("made of fossilized wood blocks");
                       tmparray.add("small hatches set into the trunks of trees");
                       tmparray.add("tunnel mouths, opening beside an impossible stream of water that runs backward into the earth");
                   }
               }

               else if(j == Themes.Library) {
                   if (k == Room_part.floor || k == Room_part.ceiling || k == Room_part.walls) {
                       tmparray.add("empty wooden shelves, here and there you can see scratch marks\n as if someone desperate " +
                               "had tried to claw a way out");
                       tmparray.add("stacks of enormous leather bound... Tatooed leather?! \nFolios, each one as tall as you " +
                               "are and trying to lure you into opening it\n with promesses of untold wonders");
                       tmparray.add("books scattered all over, held together by some mystic force preventing you from moving any...");
                       tmparray.add("gigantic stone shelves filled with clay tablets\n and scrolls slowly turning to dust");
                       tmparray.add("half burnt books and scrolls");
                       tmparray.add("mirrors... Mirrors? you can see yourself in them that's for sure \nbut as of now you are" +
                               " not reading a book and savouring a glass of brandy!");
                       tmparray.add("books chained and cadenassed shut, some seem to be asleep. \nThe others lash out as they...See" +
                               " you? making clapping noises with their covers. \nYou think that one had teeth..?");
                       tmparray.add("old shelves filled with dust and leathery bits, the odds and ends of what must \n" +
                               "have been books at some point");
                   }
                   else if(k == Room_part.atmosphere){
                       tmparray.add("is as dry and dead as a tomb's");
                       tmparray.add("is incongruently festive, it feels as if someone just laughed at a good joke even if\n" +
                               " you are alone here... \nalmost as if a party just ended before you got here");
                       tmparray.add("is studious, busy even though no one is here");
                   }
                   else{
                       tmparray.add("made of an old and unbelievably heavy wood that seems as fireproof as... disapproving?");
                       tmparray.add(" of the reinforced vault kind, engraved with cabbalistic symbols that writh as if trying" +
                               " to escape your gaze");
                   }
               }
               else if (j == Themes.FlyingDutchman){
                   if(k == Room_part.floor || k == Room_part.walls || k == Room_part.ceiling){
                       tmparray.add("sea water soaked planks");
                       tmparray.add("driftwood");
                       tmparray.add("rotten and saltwater sodden planks");
                   }
                   else if (k == Room_part.atmosphere){
                       tmparray.add("is piratey, you can hear shanty songs in other rooms");
                       tmparray.add("is sinister, you hear the boards creak as if wailing and in mourn");
                       tmparray.add("is meteorogically terrifying, it must be quite a storm out there\n you can hear" +
                               "sea men running on the deck above you");
                   }
                   else{
                       tmparray.add("a boat's hatch");
                   }
               }
               else if (j == Themes.Hindenburg){
                   if(k == Room_part.ceiling){
                       tmparray.add("cables and strange helium filled balloons");
                   }
                   else if (k == Room_part.floor || k == Room_part.walls){
                       tmparray.add("preciously varnished and decorated wooden planks");
                       tmparray.add("burning wooden planks with spots that seem to still hold together\n despite the odds");
                   }
                   else if (k == Room_part.atmosphere){
                       tmparray.add("is full of smoke and a sent of burnt wood and leaking gas");
                   }
                   else{
                       tmparray.add("a beautiful specimen of victorian style art");
                   }
               }
               tmpentry.put(k,tmparray);
           }
            Room_Description.components.put(j,tmpentry);
        }
    }
}

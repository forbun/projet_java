package dungeon_building;

import character.Monster;
import items.Item;

import java.util.ArrayList;

/**
 * Created by root on 20/10/15.
 */
public class Room extends Trap_Host {
    private int portes_cachees;
    private ArrayList<Door> doors;
    private ArrayList<Item> coffer;
    private Room_Description description;
    private ArrayList<Monster> monsters;
    private ArrayList<Switch> aSwitches;

    public Room(Place level){
        this.doors = new ArrayList<Door>();
        this.monsters = new ArrayList<Monster>();
        this.aSwitches = new ArrayList<Switch>();
        this.level = level;
        this.coffer = new ArrayList<Item>();
        this.portes_cachees = 0;
        this.description = new Room_Description(this);
        if(this.level.dungeon.build_rng() % (10-this.level.get_difficulty()) == 0){
            this.trap = new Trap(1+this.level.get_difficulty(),this.level.get_difficulty());
        }
        else{
            this.trap = null;
        }
    }

    public void add_to_coffer(Item object){
        this.coffer.add(object);
    }

    public ArrayList<Item> loot_room(){
        return this.coffer;
    }

    public int get_nb_portes_cachees(){
        return this.portes_cachees;
    }

    public int get_nb_portes(){
        return this.doors.size();
    }

    public int get_nb_visible_doors() {
        int res = 0;
        for (Door d : this.doors) {
            res += (d.hidden ? 0 : 1);
        }

        return res;
    }

    public ArrayList<Door> get_visible_doors() {
        ArrayList<Door> res = new ArrayList<Door>();

        for (Door d : this.doors) {
            if(!d.hidden) {
                res.add(d);
            }
        }

        return res;
    }

    public int get_nb_monsters(){
        return this.monsters.size();
    }

    public Monster get_n_monster(int n){
        return this.monsters.get(n);
    }

    public void del_monster(Monster mon){
        for(int i = 0; i < this.monsters.size();i++){
            if(this.monsters.get(i) == mon){
                this.monsters.remove(mon);
                break;
            }
        }
    }

    public void add_monster(Monster mon){
        this.monsters.add(mon);
    }

    public void inc_hidden_door(int n){
        this.portes_cachees+= n;
    }

    public int get_nb_interrupteurs(){
        return this.aSwitches.size();
    }

    public Switch get_n_inter(int n){
        return this.aSwitches.get(n);
    }

    public boolean are_monsters_there(){
        return this.monsters.size() != 0;
    }

    public Monster get_rand_monster(){
        return this.monsters.get(this.level.dungeon.combat_rng() % this.monsters.size());
    }

    public void add_interrupteur(Switch inter){
        this.aSwitches.add(inter);
    }

    public void add_door(Door d){
        this.doors.add(d);
    }

    public Door get_entry(){
        return this.doors.get(0);
    }

    public Door get_exit(){
        return this.doors.get(1);
    }

    public Door get_n_door(int n){
        return this.doors.get(n);
    }

    public String get_description(){
        String res = "The room's atmosphere "+this.description.atmosphere+" ,\n the "+this.get_nb_visible_doors()+
                " doors you can see are " +
                this.description.doors+" \nthe floor is made of "+this.description.floor +
                " \nthe ceiling is made of "+this.description.ceiling+".\n As you gaze upon the walls you see that they are "+
                "made of "+this.description.walls+" you can see "+this.aSwitches.size()+" levers in this room.\n" +
                "more might be hidden though...";
        if(this.are_monsters_there()){
            res+="\n \n you can see some movement in the dark corners of the room\n";
        }
        if(this.get_entry().get_joining_rooms()[0] == null) {//this is the first room of the level
            res += "\n\nBeware, trying to go back will lead to certain death!";
        }
        return res;
    }
}

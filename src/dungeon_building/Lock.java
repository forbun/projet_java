package dungeon_building;

import items.DoorKey;

/**
 * Created by root on 21/10/15.
 */
public class Lock implements Activable {
    private boolean engaged;
    private Switch aSwitch;
    private boolean hidden;
    private items.DoorKey key;

    public Lock(boolean hidden, Switch inter, DoorKey key){
        this.engaged = true;
        this.hidden = hidden;
        this.aSwitch = inter;
        this.key = key;
    }

    public boolean lock_me(){
        if(this.aSwitch == null && this.engaged == false){
            this.engaged = true;
        }
        return this.engaged;
    }

    public boolean unlock_me(){
        if(this.aSwitch == null && this.engaged == true){
            this.engaged = false;
        }
        return !this.engaged;
    }

    public boolean activate(){
        this.engaged = true;
        return this.engaged;
    }

    public boolean deactivate(){
        this.engaged = false;
        return !this.engaged;
    }

    public boolean get_state(){
        return this.engaged;
    }
}

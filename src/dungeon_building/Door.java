package dungeon_building;

import dungeon_building.exceptions.DoorException;

/**
 * Created by root on 21/10/15.
 */
public class Door extends Trap_Host implements Activable{
    private Room room_a;
    private Room room_b;
    public Lock lock;
    public boolean secret;//if this is a secret door it is hidden when closed
    public boolean is_death;//opening this door will result in insta death
    private boolean closed;
    public boolean hidden;
    public int hiding_score;
    public Switch aSwitch;

    public Door(Room a, Room b, boolean hidden, Lock lock, Trap piege, Place level){
        this.closed = true;
        this.room_a = a;
        this.room_b = b;
        this.secret = hidden;
        this.hidden = hidden;
        this.lock = lock;
        this.trap = piege;
        this.level = level;
        this.hiding_score = hidden?this.level.dungeon.difficulty:0;
        this.is_death = false;//level != null?level.dungeon.build_rng() % 100000 == 0:false;//one chance in a hundred to have a death door
    }


    private void update_hidden(){
        if(this.secret && !this.closed){
            this.hidden = false;
        }
        else if (this.secret && this.closed){
            this.hidden = true;
        }
    }

    public boolean open() throws DoorException {
        if((this.lock == null || this.lock.get_state() == false)&&this.aSwitch == null) {
            this.closed = false;
        }
        if(this.lock != null && this.lock.get_state() == true){
            throw new DoorException("can not open a locked door, unlock first!");
        }
        else if(this.aSwitch != null){
            throw new DoorException("can not open this way, you should use the switch object");
        }
        this.update_hidden();
        return !this.closed;
    }

    public boolean close() throws DoorException {
        if(this.closed == false && this.aSwitch == null){
            this.closed = true;
        }
        if(this.aSwitch != null){
            throw new DoorException("can not close this way, use the switch object");
        }
        if(this.lock.get_state() == true){
            throw new DoorException("can not close a door that has en engaged lock, disengage lock first");
        }

        this.update_hidden();
        return this.closed;
    }

    public boolean activate()throws DoorException {
        if(this.lock == null||this.lock.get_state() == false){
            System.out.println("Somewhere, a door opens...");
            this.closed = false;
        }
        if(this.lock.get_state() == true){
            throw new DoorException("can not close a door that has en engaged lock, disengage lock first");
        }
        this.update_hidden();
        return !this.closed;
    }

    public boolean deactivate() throws DoorException {
        if(this.lock == null || this.lock.get_state() == false){
            System.out.println("Somewhere, a door closes...");
            this.closed = true;
        }
       if(this.lock.get_state() == true){
            throw new DoorException("can not close a door that has en engaged lock, disengage lock first");
        }
        this.update_hidden();
        return this.closed;
    }

    public Room[] get_joining_rooms(){
        Room[] rooms = new Room[2];
        rooms[0] = this.room_a;
        rooms[1] = this.room_b;
        return rooms;
    }

    public boolean is_closed(){
        return this.closed;
    }

}

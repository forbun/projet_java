package dungeon_building.enumerations;

import dungeon_building.Dungeon;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by root on 20/10/15.
 */
public enum Themes {
    Cave,
    Lava,
    Jungle,
    Library,
    FlyingDutchman,
    Hindenburg;

    private static final List<Themes> VALUES =
            Collections.unmodifiableList(Arrays.asList(values()));
    private static final int SIZE = VALUES.size();

    public static Themes random_Theme(Dungeon D)  {
        return VALUES.get(Math.abs(D.build_rng() % VALUES.size()));
    }
}

package dungeon_building.enumerations;

/**
 * Created by root on 27/10/15.
 */
public enum Room_part {
    floor,
    walls,
    ceiling,
    doors,
    atmosphere;
}

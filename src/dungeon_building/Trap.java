package dungeon_building;

import character.Entity;
import dungeon_building.exceptions.ActivableException;

/**
 * Created by root on 20/10/15.
 */
public class Trap implements Activable {
    private int damage;
    private int hiding_score;
    private String description;
    public Trap(int damage,int score){
        this.damage = damage;
        this.hiding_score = score;
    }

    public int get_damage(){
        return this.damage;
    }

    public int get_score(){
        return this.hiding_score;
    }

    public void spring(Entity target){
        System.out.println("Ouch, you sprung a trap, you lose "+this.damage+" health points!");
        target.loseLifePoints(this.damage);
    }
    public boolean activate() throws ActivableException {return true;}
    public boolean deactivate() throws ActivableException{return true;}
}

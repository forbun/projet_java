package dungeon_building;

import character.*;
import items.Item;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by root on 02/11/15.
 */
public class Save implements  Serializable{
    private String name;
    private String nameCh;
    private Races race;
    private Professions prof;
    private int char_level;
    private int level;
    private int level_seed;
    private ArrayList<Item> bag;

    public Save(String name, Player mycharacter){
        long jour = System.currentTimeMillis();
        this.name = name+jour;
        this.level = mycharacter.getLocation().level.getLevel_number();
        this.level_seed = mycharacter.getLocation().level.getGen_seed();
        this.nameCh = mycharacter.getName();
        this.race = mycharacter.getRace_template();
        this.prof = mycharacter.getProf_template();
        this.char_level = mycharacter.getLevel();
        this.bag = mycharacter.get_bag();
    }

    public void set_name(String newname){
        this.name = newname;
    }
    public Player get_player(){
        Player mychar = new Player(this.nameCh,this.race,this.prof,this.char_level);
        return mychar;
    }

    public int get_level_seed(){
        return this.level_seed;
    }

    public int get_level(){
        return this.level;
    }

    public Save(String filename) throws IOException, ClassNotFoundException {
            FileInputStream fileIn = new FileInputStream("/tmp/"+filename);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            Save tmp = (Save) in.readObject();
            in.close();
            fileIn.close();
            this.name = tmp.name;
            this.level_seed = tmp.level_seed;
            this.level = tmp.level;
            this.race = tmp.race;
            this.prof = tmp.prof;
    }

    public void dump(){
        String filename = this.name;
        try
        {
            FileOutputStream fileOut =
                    new FileOutputStream("/tmp/"+filename);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(this);
            out.close();
            fileOut.close();
            System.out.printf("game saved in /tmp/"+filename+"\n");
        }
        catch(IOException i)
        {
            i.printStackTrace();
        }
    }

}

package dungeon_building.exceptions;

/**
 * Created by root on 21/10/15.
 */
public abstract class ActivableException extends Exception {
    public ActivableException() { super(); }
    public ActivableException(String message) { super(message); }
    public ActivableException(String message, Throwable cause) { super(message, cause); }
    public ActivableException(Throwable cause) { super(cause); }
}

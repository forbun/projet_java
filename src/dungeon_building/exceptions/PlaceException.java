package dungeon_building.exceptions;

/**
 * Created by root on 21/10/15.
 */
public class PlaceException extends Exception {
    public PlaceException() { super(); }
    public PlaceException(String message) { super(message); }
    public PlaceException(String message, Throwable cause) { super(message, cause); }
    public PlaceException(Throwable cause) { super(cause); }
}

package dungeon_building.exceptions;

/**
 * Created by root on 21/10/15.
 */
public class DoorException extends ActivableException {
    public DoorException() { super(); }
    public DoorException(String message) { super(message); }
    public DoorException(String message, Throwable cause) { super(message, cause); }
    public DoorException(Throwable cause) { super(cause); }
}

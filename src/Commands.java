
/** Commands store all needed commands in this game.
 *
 *  Each command have :<ul>
 *  <li>The first word is the command</li>
 *  <li>The number of arguments of this command</li>
 *  <li>A brief description of this, which is show with the 'help' command</li>
 *  </ul>
 */
public enum Commands {
    EQUIP    ("equip"  , 1, "equip <item>     : equip a Wearable item."),
    GO       ("go"     , 1, "go <idDoor>      : try to go in the next room."),
    HELP     ("help"   , 0, "help             : show this help."),
    INSPECT  ("inspect", 0, "inspect          : look if there is more in the room."),
    LOOK_BAG ("lookBag", 0, "lookBag          : show all items in the player's bag."),
    LOOK_ITEM("look"   , 1, "look <itemName>  : take a description of the item."),
    LOOK_ME  ("lookMe" , 0, "lookMe           : show the player."),
    LOOK_ROOM("look"   , 0, "look             : take a description of the room."),
    QUIT     ("quit"   , 0, "quit             : quit the game."),
    SAVE     ("save"   , 1, "save <file>      : save the game in the file."),
    TAKE     ("take"   , 1, "take <item>      : add the item to the player's bag."),
    UNEQUIP  ("unequip", 1, "unequip <body>   : remove the item on selected body part."),
    USE      ("use"    , 1, "use <item>       : use the item."),
    USE_ON   ("use"    , 2, "use <item> <obj> : use the item on the object.");

    private final int nbParameters;
    private final String command;
    private final String helpText;

    Commands(String command, int nbParameters, String helpText) {
        this.command = command;
        this.nbParameters = nbParameters;
        this.helpText = helpText;
    }

    public int getNbParameters() {
        return this.nbParameters;
    }

    public String getCommand() {
        return this.command;
    }

    public String getHelpText() {
        return this.helpText;
    }
}

package character;

import character.races.*;
import dungeon_building.Dungeon;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/** Races is a enum which list all races.
 *
 * This class simplify the creation of an Entity : you only need to pass
 *  in the constructor the enum of the class. The constructor does the rest.
 */
public enum Races {
    ORC(new Orc()),
    GOBLIN(new Goblin()),
    HUMAIN(new Humain()),
    NAIN(new Nain()),
    ELF(new Elf());

    Race _race;

    Races(Race race) {
        _race = race;
    }

    /** Get the corresponding Race.
     *
     */
    Race getRace() {
        return _race;
    }
    private static final List<Races> VALUES =
            Collections.unmodifiableList(Arrays.asList(values()));
    private static final int SIZE = VALUES.size();

    /** Get a random Race, with the rng of the Dungeon d.
     *
     */
    public static Races random_race(Dungeon D)  {
        return VALUES.get(Math.abs(D.build_rng() % VALUES.size()));
    }
}

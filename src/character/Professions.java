package character;

import character.professions.*;
import dungeon_building.Dungeon;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/** Professions is a enum which list all professions.
 *
 * This class simplify the creation of an Entity : you only need to pass
 *  in the constructor the enum of the class. The constructor does the rest.
 */
public enum Professions {
    MAGE(new Mage()),
    ROGUE(new Rogue()),
    WARRIOR(new Warrior());

    private Profession _profession;

    Professions(Profession prof) {
        _profession = prof;
    }

    /** Get the corresponding Profession.
     *
     */
    Profession getProfession() {
        return _profession;
    }

    private static final List<Professions> VALUES =
            Collections.unmodifiableList(Arrays.asList(values()));
    private static final int SIZE = VALUES.size();

    /** Get a random Profession, with the rng of the Dungeon d.
     *
     */
    public static Professions random_profession(Dungeon D)  {
        return VALUES.get(Math.abs(D.build_rng() % VALUES.size()));
    }
}

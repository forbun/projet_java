package character;

import dungeon_building.Room;

/**
 * Created by root on 20/10/15.
 */
public class Monster extends Entity {
    public Monster(String name, Races race, Professions prof, Room location) {
        super(name, race, prof,location);
        this.quick_description = race.getRace().get_description()+prof.getProfession().get_description();
    }

    public Monster(String name, Races race, Professions prof, int level,boolean friendly,Room location) {
        super(name, race, prof, level);
        this.is_friendly = friendly;
        this.location = location;
        this.quick_description = race.getRace().get_description()+prof.getProfession().get_description();
    }
}

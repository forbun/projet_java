package character;

/** Profession is the root class for all professions.
 *
 */
public abstract class Profession {
    private final float _bonusStrength;
    private final float _bonusDexterity;
    private final float _bonusIntelligence;
    private final float _bonusLifePoints;
    private final float _bonusManaPoints;
    private final String description="";

    /** The constructor, which ask all bonus for the Race.
     *
     * This is useful for inherited classes.
     *
     * Each bonus is a multiplier.
     */
    public Profession(float dexterity, float intelligence, float strength, float lifePts, float manaPts) {
        _bonusDexterity = dexterity;
        _bonusIntelligence = intelligence;
        _bonusLifePoints = lifePts;
        _bonusManaPoints = manaPts;
        _bonusStrength = strength;
    }

    /** Get the bonus of max Life for this Profession.
     *
     */
    public float getBonusLifePoints() {
        return _bonusLifePoints;
    }

    /** Get the bonus of max mana for this Profession.
     *
     */
    public float getBonusManaPoints() {
        return _bonusManaPoints;
    }

    /** Get the bonus of strength for this Profession.
     *
     */
    public float getBonusStrength() {
        return _bonusStrength;
    }

    /** Get the bonus of dexterity for this Profession.
     *
     */
    public float getBonusDexterity() {
        return _bonusDexterity;
    }

    /** Get the bonus of intelligence for this Profession.
     *
     */
    public float getBonusIntelligence() {
        return _bonusIntelligence;
    }

    public String get_description(){
        return this.description;
    }
}
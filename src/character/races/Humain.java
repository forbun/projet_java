package character.races;

import character.Race;

/**
 * Created by root on 28/10/15.
 */
public class Humain extends Race{
    private final String description = "A human";
    public Humain() {
        super(1f, 1f, 1f, 1f, 1f);
    }
    public String get_description(){
        return this.description;
    }
}

package character.races;

import character.Race;

/**
 * Created by root on 28/10/15.
 */
public class Elf extends Race {
    private final String description = "A slender and beautiful being with pointy ears";
    public Elf(){

        super(2.0f, 2.0f, 0.5f, 0.5f, 2.0f);
    }
    public String get_description(){
        return this.description;
    }
}

package character.races;

import character.Race;

/**
 * Created by root on 28/10/15.
 */
public class Nain extends Race{
    private final String description = "A midget with homicidal mania and serious issues with his alcohol consumption";
    public Nain(){
        super(1.5f,1f,1.5f,1.5f,0.5f);
    }
    public String get_description(){
        return this.description;
    }
}

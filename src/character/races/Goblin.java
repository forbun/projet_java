package character.races;

import character.Race;

/**
 * Created by root on 28/10/15.
 */
public class Goblin extends Race{
    private final String description = "A small, vicious looking, ugly and green ape like being";
    public Goblin(){
        super(2f,0.5f,0.25f,0.25f,1f);
    }
    public String get_description(){
        return this.description;
    }
}

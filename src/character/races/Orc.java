package character.races;

import character.Race;

/**
 * .
 *
 * @author bunlang
 */
public class Orc extends Race {

    private final String description = "A big green ogrish ape, with cleavers and necklaces of ears";
    public Orc() {
        super(1.16f, 0.66f, 1.17f, 1.33f, 0.67f);
    }
    public String get_description(){
        return this.description;
    }
}

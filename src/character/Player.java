package character;

import dungeon_building.Door;
import dungeon_building.Dungeon;
import dungeon_building.Switch;
import dungeon_building.Room;
import dungeon_building.Trap_Host;
import dungeon_building.exceptions.DoorException;
import items.Item;

import java.util.ArrayList;
import java.util.HashMap;

/** Represents a player.
 *
 */
public class Player extends Entity {
    /** A constructor, with the minimal information.
     *
     */
    public Player(String name, Races race, Professions prof) {
        super(name, race, prof,null);
    }

    /** A constructor, with the level of the Player.
     *
     */
    public Player(String name, Races race, Professions prof, int level) {
        super(name, race, prof, level);this.is_friendly = true;
        this.quick_description = race.getRace().get_description()+prof.getProfession().get_description();
    }

    /** Serch in the Room cur some hidden content (door, switcher).
     *
     *
     */
    public void search_room(Room cur){//reveals doors and traps when entering a room, depending on intelligence
        for(int i = 0; i < cur.get_nb_portes();i++){
            Door d = cur.get_n_door(i);
            if(d.hidden){
                boolean washidden = d.secret;
                d.secret = cur.level.dungeon.combat_rng() % 100 < this.getIntelligence()-d.hiding_score;
                d.hidden = d.secret;
                if(washidden && !d.secret && d.get_joining_rooms()[0]!=null){
                    d.get_joining_rooms()[0].inc_hidden_door(-1);//reduce the number of hidden doors
                }
            }
        }
        for(int i = 0; i < cur.get_nb_interrupteurs();i++){
            Switch curint = cur.get_n_inter(i);
            if(curint.hidden){
                curint.hidden = cur.level.dungeon.combat_rng() % 100 < this.getIntelligence()-curint.hiding_score;
            }
        }

    }

    /** Try to deactivate the cur Trap.
     *
     */
    private void examine_for_trap(Trap_Host cur){
        if(cur.trap != null){
            if(cur.level.dungeon.combat_rng() % 100 < this.getIntelligence()-cur.trap.get_score()){
                cur.trap.spring(this);
            }
            else{
                System.out.println("you found a trap and deactivated it!");
                cur.trap = null;
            }
        }
    }

    /** Try to open the Door d, when the player is in the Room currentRoom?
     *
     * @throws DoorException The door can't be opened.
     */
    public void open_door(Door d, Room currentRoom) throws DoorException {
        this.search_door(d);
        if(d.lock != null && d.lock.get_state()){
            System.out.println("can not open that door, its locked!");
        }
        else if (d.aSwitch != null){
            System.out.println("can not open that door : its remotely activated!");
        }
        else if(d.is_death){
            System.out.println("you are sucked by the wind into a bottomless pit. As you fall you can hear a cackling voice\n" +
                    "laughing. you will keep passing signs with 'bunlanG' written on it until you die of old age\n" +
                    "or your heart gives out");
            this.loseLifePoints(this.getLifePoints());
        }
        else{
            d.open();
            if(d.get_joining_rooms()[1] == null){//last room for the level
                Room nextroom = this.location.level.dungeon.go_to_level(this.location.level.getLevel_number()+1);
                System.out.println("you descend further down in the dungeon... Or is it ascending?");
                this.location = nextroom;
            }
            else {
                this.go_to_next_room(d, currentRoom);
            }
        }
    }

    /** Set the location of the Player : the first level of the dungeon d.
     *
     */
    public void goToStart(Dungeon d) {
        if(this.location == null) {
            this.location = d.go_to_level(0);
        } else {

        }
    }

    /** Go to another room, behind the Door d, when the Player is in currentRoom.
     *
     */
    public void go_to_next_room(Door d, Room currentRoom){
        Room[] joiningRooms = d.get_joining_rooms();
        if(joiningRooms[0] == currentRoom) {
            this.location = d.get_joining_rooms()[1];
        } else if(joiningRooms[1] == currentRoom) {
            this.location = d.get_joining_rooms()[0];
        }

        System.out.println("\nYou pass the door, and you enter in a new room :");
        if(this.location.are_monsters_there()){
            ArrayList<Entity> friendlies = new ArrayList<Entity>();
            ArrayList<Entity> foes = new ArrayList<Entity>();
            HashMap<Integer,Entity> initiative = new HashMap<Integer,Entity>();

            friendlies.add(this);//adds the player as friendly
            initiative.put(this.get_initiative(), this);


            System.out.println("as you enter the room you can see:");
            for(int i = 0; i < this.location.get_nb_monsters();i++){
                Monster cur = this.location.get_n_monster(i);
                Integer init = cur.get_initiative();
                System.out.println(cur.quick_description);
                if(cur.is_friendly){
                    friendlies.add(cur);
                }
                else{
                    foes.add(cur);
                }
                while(initiative.get(init)!= null){
                    init++;
                }
                initiative.put(init,cur);
                cur = this.location.get_n_monster(i);
            }
            System.out.println("seems you are going to have to fight your way out!");
            Entity.fight(friendlies, foes, initiative);
            ArrayList<Item> loot = this.location.loot_room();
            if(loot.size() > 0){
                System.out.println("You found:\n");
                for(int i = 0; i < loot.size();i++){
                    this.get_bag().add(loot.get(i));
                    System.out.println(loot.get(i).toString());

                }
            }

        }
        this.examine_for_trap(this.location);
        this.search_room(this.location);
    }

    /** Search a trap on the Door d.
     *
     */
    public void search_door(Door d){
        examine_for_trap(d);
    }
}

package character;

/**
 * .
 *
 * @author bunlang
 */
public enum BodyPart {
    HAND_RIGHT("Right Hand"),
    HAND_LEFT("Left Hand"),
    FINGER("Finger"),
    ARMS("Arms"),
    HEAD("Head"),
    CHEST("Chest"),
    LEGS("Legs");

    private String _name;

    BodyPart(String name) {
        _name = name;
    }

    @Override
    public String toString() {
        return _name;
    }
}

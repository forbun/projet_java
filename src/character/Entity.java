package character;

import character.exceptions.UnconsumableException;
import items.*;

import java.util.ArrayList;
import java.util.HashMap;
import dungeon_building.Dungeon;
import dungeon_building.Room;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.SortedSet;
import java.util.TreeSet;

/** Represents all living beings.
 *
 *
 */
public abstract class Entity {
    
    // Fields
    private final String _name;
    private final Profession _profession;
    private final Race _race;
    private final Races race_template;
    private final Professions prof_template;
    private final int _baseStrength;
    private final int _baseDexterity;
    private final int _baseIntelligence;
    private final int _baseLifePoints;
    private final int _baseManaPoints;
    private int _fixedBonusStrength;
    private int _fixedBonusDexterity;
    private int _fixedBonusIntelligence;
    private int _fixedBonusLifePoints;
    private int _fixedBonusManaPoints;
    public boolean is_friendly;

    protected Room location;
    private int _level;
    private int _xp;
    private int _currLifePoints;
    private int _currManaPoints;
    public String quick_description;

    private ArrayList<Item> _bag;
    private HashMap<BodyPart, Wearable> _equipment;

    /** A constructor, with the current location of the Entity.
     *
     */
    public Entity(String name, Races race, Professions prof,Room location) {
        this(name, race, prof, 1);
        this.location = location;
    }

    /** A constructor, with the level of the Entity.
     *
     */
    public Entity(String name, Races race, Professions prof, int level) {
        _name = name;        
        this.prof_template = prof;
        this.race_template = race;
        _equipment = new HashMap<BodyPart, Wearable>();
        _bag = new ArrayList<Item>();
        _race = race.getRace();
        _profession = prof.getProfession();
        _level = level;
        _xp = levelToXp(level);

        _baseStrength = (int) (100 * _profession.getBonusStrength()
                                   * _race.getBonusStrength());
        _baseDexterity = (int) (100 * _profession.getBonusDexterity()
                                    * _race.getBonusDexterity());
        _baseIntelligence = (int) (100 * _profession.getBonusIntelligence()
                                       * _race.getBonusIntelligence());
        _baseLifePoints = (int) (100 * _profession.getBonusLifePoints()
                                     * _race.getBonusLifePoints());
        _baseManaPoints = (int) (100 * _profession.getBonusManaPoints()
                                     * _race.getBonusManaPoints());

       _fixedBonusStrength = 0;
       _fixedBonusDexterity = 0;
       _fixedBonusIntelligence = 0;
       _fixedBonusLifePoints = 0;
       _fixedBonusManaPoints = 0;

        _currLifePoints = getMaxLifePoints();
        _currManaPoints = getMaxManaPoints();
        this._bag.add(Item.get_rand_item(level));
        location = null;
    }

    /** Convert level into the corresponding experience.
     *
     * <p>That's the experience when the entity just reach the level.</p>
     *
     * <p>That's a geometric series, with a ratio of 1.15 (+15% of xp needed
     * per level), and the level 2 is reached with 1.000 xp.
     * </p>
     *
     */
    public static int levelToXp(int level) {
        return (int) Math.ceil(1000 * (Math.pow(1.15,(float)(level - 1)) - 1)
                                              /(0.15));
    }

    /** Convert experience into the corresponding level.
     *
     */
    public static int xpToLevel(int exp) {
        return (int) Math.floor(1 + (Math.log(0.00015 * exp + 1))
                                             /(Math.log(1.15)));
    }

    /** Add some experience.
     *
     */
    public void addXP(int xpGained) {
        if(xpGained > 0) {
            _xp += xpGained;
            int newLevel = xpToLevel(_xp);
            // Compare both levels
            if(newLevel != _level) {
                System.out.println("  Level up !");
                _level = newLevel;
            }
        }
    }

    /** Add an Item in the bag.
     *
     * @return true (as specified by Collection.add(E))
     */
    public boolean addItem(Item item) {
        return _bag.add(item);
    }

    /** Get an Item with its name.
     *
     * @return an Item with the good name, or null if it doesn't exist.
     */
    public Item getItemByName(String name) {
        for(Item i : _bag) {
            if(i.getName().equals(name)) {
                return i;
            }
        }

        return null;
    }

    /** Remove the item from the bag.
     *
     * @return true if this bag contained the specified item
     */
    public boolean dropItem(Item item) {
        return _bag.remove(item);
    }

    /** Equip the Wearable item on the entity.
     *
     * @return the previous Wearable on the BodyPart
     */
    public Wearable equip(Wearable item) {
        ArrayList<BodyPart> allowed = item.getAllowedParts();

        // remove LIFE and MANA bonuses (only once)
        Integer bonusLife = item.removeEffect(Affected.LIFE);
        Integer bonusMana = item.removeEffect(Affected.MANA);
        if(bonusLife != null) {
            _currLifePoints = Math.min(_currLifePoints + bonusLife, getMaxLifePoints());
        }
        if(bonusMana != null) {
            _currLifePoints = Math.min(_currLifePoints + bonusMana, getMaxLifePoints());
        }

        // Add item on the entity
        if(allowed.size() > 0) {
            return _equipment.put(allowed.get(0), item);
        } else {
            return null;
        }

    }

    /** Clear the BodyPart.
     *
     * @return the previous Wearable on the BodyPart
     */
    public Wearable unequip(BodyPart bp) {
        return _equipment.remove(bp);
    }

    /** Use a consumable.
     *
     */
    public void consume(Consumable cons) throws UnconsumableException {
        if(cons instanceof Item) {
            Item item = (Item) cons;
            if (_bag.contains(item)) {

                dropItem((Item) item);
                // Apply effects on entity
                for(Affected aff : Affected.values()) {
                    Integer bonus = item.getEffect(aff);
                    if(bonus != null) {
                        switch (aff) {
                            case LIFE:
                                // Check if we exceed MAX_LIFE
                                _currLifePoints = Math.min(_currLifePoints + bonus, getMaxLifePoints());
                                break;
                            case MANA:
                                // Check if we exceed MAX_LIFE
                                _currManaPoints = Math.min(_currManaPoints + bonus, getMaxManaPoints());
                                break;
                            case STRENGTH:
                                _fixedBonusStrength += bonus;
                                break;
                            case DEXTERITY:
                                _fixedBonusDexterity += bonus;
                                break;
                            case INTELLIGENCE:
                                _fixedBonusIntelligence += bonus;
                                break;
                            case MAX_LIFE:
                                _fixedBonusLifePoints += bonus;
                                break;
                            case MAX_MANA:
                                _fixedBonusManaPoints += bonus;
                                break;
                        }
                    }
                }
            } else {
                throw new UnconsumableException("This item is not in your bag.");
            }
        } else {
            throw new UnconsumableException("This object is not a item.");
        }
    }

    /** Decrease life of lifePointsLost.
     *
     */
    public void loseLifePoints(int lifePointsLost) {
        _currLifePoints -= lifePointsLost;
        if(_currLifePoints <= 0 && this instanceof Player){
            System.out.println("you died! thanks for playing\n the seed was:" +
                    this.location.level.dungeon.get_seed());
            System.exit(0);
        }
        else if(_currLifePoints <= 0){
            for(int i = 0; i < this._bag.size();i++){
                Item cur = this._bag.get(i);
                this.location.add_to_coffer(cur);
            }
        }
    }

    /** Decrease mana of manaUsed.
     *
     */
    public void useMana(int manaUsed) {
        _currManaPoints -= manaUsed;
    }

    /** Check if the entity can consume manaNeeded.
     */
    public boolean haveEnoughMana(int manaNeeded) {
        return (getManaPoints() >= manaNeeded);
    }

    /** Check if the entity is dead.
     */
    public boolean isDead() {
        return (getLifePoints() <= 0);
    }

    /** Check if the entity have not mana.
     */
    public boolean haveNoMana() {
        return (getManaPoints() <= 0);
    }

    /** Get the current life.
     *
     */
    public int getLifePoints() {
        return _currLifePoints;
    }

    /** Get the current mana.
     *
     */
    public int getManaPoints() {
        return _currManaPoints;
    }

    /** Get the maximum of life.
     *
     * Each characteristic depends of several things :<ul>
     *  <li>some basic life, which depends of the Race and the Profession</li>
     *  <li>an multiplier, which depends of the level (add 5% of the previous level)</li>
     *  <li>a bonus, depending of the equipment</li>
     *  <li>another bonus, depending of all consumed Item</li>
     *  </ul>
     */
    public int getMaxLifePoints() {
        return (int) (_baseLifePoints * Math.pow(1.05, _level - 1)) + getBonus(Affected.MAX_LIFE)
                + _fixedBonusLifePoints;
    }

    /** Get the maximum of mana.
     *
     */
    public int getMaxManaPoints() {
        return (int) (_baseManaPoints * Math.pow(1.05, _level - 1)) + getBonus(Affected.MAX_MANA)
                + _fixedBonusManaPoints;
    }

    /** Get the strentgh.
     *
     */
    public int getStrength() {
        return (int) (_baseStrength * Math.pow(1.05, _level - 1)) + getBonus(Affected.STRENGTH)
                + _fixedBonusStrength;
    }

    /** Get the dexterity.
     *
     */
    public int getDexterity() {
        return (int) (_baseDexterity * Math.pow(1.05, _level - 1)) + getBonus(Affected.DEXTERITY)
                + _fixedBonusDexterity;
    }

    /** Get the intelligence.
     *
     */
    public int getIntelligence() {
        return (int) (_baseIntelligence * Math.pow(1.05, _level - 1)) + getBonus(Affected.INTELLIGENCE)
                + _fixedBonusIntelligence;
    }

    /** Get the bonus for a Affected (life, mana, strength...).
     *
     * This bonus includes the equipment bonus and the consumed Item bonus.
     *
     */
    public int getBonus(Affected aff) {
        int res = 0;
        Integer tmp; // Store the return of get() (may be null)

        // Bonus with equipment
        for(BodyPart bp : _equipment.keySet()) {
            tmp = _equipment.get(bp).getEffect(aff);

            if(tmp != null) {
                res += tmp;
            }
        }

        // Add fixed bonus, because of Consumed Item
        switch(aff) {
            case LIFE:
                // No.
                break;
            case MANA:
                // No.
                break;
            case STRENGTH:
                res += _fixedBonusStrength;
                break;
            case DEXTERITY:
                res += _fixedBonusDexterity;
                break;
            case INTELLIGENCE:
                res +=_fixedBonusIntelligence;
                break;
            case MAX_LIFE:
                res += _fixedBonusLifePoints;
                break;
            case MAX_MANA:
                res += _fixedBonusManaPoints;
                break;
        }

        return res;
    }

    /** Get the level.
     *
     */
    public int getLevel() {
        return _level;
    }

    /** Get the name.
     *
     */
    public String getName() {
        return _name;
    }

    /** Get the initiative.
     *
     */
    public Integer get_initiative(){
        Integer init= Math.abs(this.location.level.dungeon.combat_rng() % 100 - this.getDexterity()-this.getIntelligence());
        return init;
    }

    public Professions getProf_template() {
        return prof_template;
    }
    public Races getRace_template() {
        return race_template;
    }

    /** Get the Race class.
     *
     */
    public Race get_race() {
        return _race;
    }

    /** Get the Profession class.
     *
     */
    public Profession get_profession() {
        return _profession;
    }

    /** Get all the bag of the Entity..
     *
     */
    public ArrayList<Item> get_bag() {
        return _bag;
    }

    /** The fight mechanism.
     *
     */
    public static void fight(ArrayList<Entity> friends, ArrayList<Entity> foes,HashMap<Integer,Entity> initiative){
        System.out.println("let the fight begin!\n its a "+friends.size()+" vs "+foes.size());
        int xpGainable = 0;
        // Calc the gainable XP before fight
        for(Entity e : foes) {
            xpGainable += e.getDexterity();
            xpGainable += e.getIntelligence();
            xpGainable += e.getStrength();
        }
        boolean fought = false;
        while(friends.size() != 0 && foes.size() != 0){
            SortedSet<Integer> keys = new TreeSet<Integer>(initiative.keySet());
            for (Integer key : keys) {
                Entity fighter = initiative.get(key);
                Dungeon D = fighter.location.level.dungeon;
                Entity target = fighter.location.get_rand_monster();
                if(fighter instanceof Player || fighter.is_friendly) {
                    while(target.is_friendly){
                        target = fighter.location.get_rand_monster();
                    }
                }
                else{
                    target = friends.get(D.combat_rng() % friends.size());
                }
                int dice_throw = D.combat_rng() % 200;
                if(dice_throw < fighter.getDexterity()){
                    int damage = fighter.getStrength() - dice_throw;
                    damage = damage > 0?damage:0;

                    if(fighter instanceof Player){
                        System.out.println("you hit "+target.quick_description);
                    }
                    else if(target instanceof Player){
                        System.out.println(fighter.quick_description+"hits you");
                    }
                    else{
                        System.out.println(fighter.quick_description+" hits the "+target.quick_description);
                    }
                    target.loseLifePoints(damage);
                    System.out.println("Making "+damage+" damage");
                    if(target.getLifePoints() <= 0) {
                        if (target.is_friendly) {
                            friends.remove(target);
                            System.out.println("your ally,\n"+target.quick_description+"\n dies messily");
                        }
                        else{
                            foes.remove(target);
                            System.out.println("the foe,\n"+target.quick_description+"\n surrenders its last breath");
                        }
                    }
                }
            }
            fought = true;
        }
        if(fought){
            System.out.println("You earned "+xpGainable+" xp.");
            // Add xp for friends
            for(Entity e : friends) {
                e.addXP(xpGainable);
            }
            System.out.println("As the dust settles you contemplate the battlefield.\n It was quite a scrap!\n" +
                    "now lets get a look at the loot...");
        }

    }

    public Room getLocation() {
        return this.location;
    }

    @Override
    public String toString() {
        String s = "";
        s += "Hi! My name is " + _name + "\n";
        s += "I'm a " + _profession.getClass().getSimpleName() + " " +
                _race.getClass().getSimpleName() + "\n";
        s += "Level : " + getLevel() + "\n";
        s += "Life : " + getLifePoints() + "/" + getMaxLifePoints() +
                " (bonus : " + getBonus(Affected.MAX_LIFE)+ ")\n";
        s += "Mana : " + getManaPoints() + "/" + getMaxManaPoints() +
                " (bonus : " + getBonus(Affected.MAX_MANA)+ ")\n";
        s += "Strength : " + getStrength() + " (bonus : " +
                getBonus(Affected.STRENGTH)+ ")\n";
        s += "Dexterity : " + getDexterity() + " (bonus : " +
                getBonus(Affected.DEXTERITY)+ ")\n";
        s += "Intelligence : " + getIntelligence() + " (bonus : " +
                getBonus(Affected.INTELLIGENCE)+ ")\n";

        // Equipment
        if(_equipment.isEmpty()) {
            s += "No equiped.\n";
        } else {
            s += "I'm equiped of :\n";

            for(BodyPart bp : _equipment.keySet()) {
                s += "On " + bp + ":\n";
                s += _equipment.get(bp);
            }

        }

        return s;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Entity)) return false;

        Entity entity = (Entity) o;

        if (_baseStrength != entity._baseStrength) return false;
        if (_baseDexterity != entity._baseDexterity) return false;
        if (_baseIntelligence != entity._baseIntelligence) return false;
        if (_baseLifePoints != entity._baseLifePoints) return false;
        if (_baseManaPoints != entity._baseManaPoints) return false;
        if (is_friendly != entity.is_friendly) return false;
        if (_level != entity._level) return false;
        if (_currLifePoints != entity._currLifePoints) return false;
        if (_currManaPoints != entity._currManaPoints) return false;
        if (!get_profession().equals(entity.get_profession())) return false;
        if (!get_race().equals(entity.get_race())) return false;
        if (getRace_template() != entity.getRace_template()) return false;
        if (getProf_template() != entity.getProf_template()) return false;
        if (!location.equals(entity.location)) return false;
        if (!quick_description.equals(entity.quick_description)) return false;
        if (!get_bag().equals(entity.get_bag())) return false;
        return _equipment.equals(entity._equipment);

    }

    @Override
    public int hashCode() {
        int result = get_profession().hashCode();
        result = 31 * result + get_race().hashCode();
        result = 31 * result + getRace_template().hashCode();
        result = 31 * result + getProf_template().hashCode();
        result = 31 * result + _baseStrength;
        result = 31 * result + _baseDexterity;
        result = 31 * result + _baseIntelligence;
        result = 31 * result + _baseLifePoints;
        result = 31 * result + _baseManaPoints;
        result = 31 * result + (is_friendly ? 1 : 0);
        result = 31 * result + location.hashCode();
        result = 31 * result + _level;
        result = 31 * result + _currLifePoints;
        result = 31 * result + _currManaPoints;
        result = 31 * result + quick_description.hashCode();
        result = 31 * result + get_bag().hashCode();
        result = 31 * result + _equipment.hashCode();
        return result;
    }
}

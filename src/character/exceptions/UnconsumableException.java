package character.exceptions;

/**
 * .
 *
 * @author bunlang
 */
public class UnconsumableException extends Exception {
    public UnconsumableException() { super(); }
    public UnconsumableException(String message) { super(message); }
    public UnconsumableException(String message, Throwable cause) { super(message, cause); }
    public UnconsumableException(Throwable cause) { super(cause); }
}

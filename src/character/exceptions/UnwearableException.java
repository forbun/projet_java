package character.exceptions;

/**
 * .
 *
 * @author bunlang
 */
public class UnwearableException extends Exception {
    public UnwearableException() { super(); }
    public UnwearableException(String message) { super(message); }
    public UnwearableException(String message, Throwable cause) { super(message, cause); }
    public UnwearableException(Throwable cause) { super(cause); }
}
package character;

/** Race is the root class for all races.
 *
 */
public abstract class Race {
    private final float _bonusStrength;
    private final float _bonusDexterity;
    private final float _bonusIntelligence;
    private final float _bonusLifePoints;
    private final float _bonusManaPoints;
    private final String description = "";

    /** The constructor, which ask all bonus for the Race.
     *
     * This is useful for inherited classes.
     *
     * Each bonus is a multiplier.
     */
    public Race(float dexterity, float intelligence, float strength, float lifePts, float manaPts) {
        _bonusDexterity = dexterity;
        _bonusIntelligence = intelligence;
        _bonusLifePoints = lifePts;
        _bonusManaPoints = manaPts;
        _bonusStrength = strength;
    }

    public String get_description(){
        return this.description;
    }

    /** Get the bonus of max Life for this Race.
     *
     */
    public float getBonusLifePoints() {
        return _bonusLifePoints;
    }

    /** Get the bonus of max mana for this Race.
     *
     */
    public float getBonusManaPoints() {
        return _bonusManaPoints;
    }

    /** Get the bonus of strength for this Race.
     *
     */
    public float getBonusStrength() {
        return _bonusStrength;
    }

    /** Get the bonus of dexterity for this Race.
     *
     */
    public float getBonusDexterity() {
        return _bonusDexterity;
    }

    /** Get the bonus of intelligence for this Race.
     *
     */
    public float getBonusIntelligence() {
        return _bonusIntelligence;
    }
}

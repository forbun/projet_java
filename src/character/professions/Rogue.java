package character.professions;

import character.Profession;

/**
 * Created by root on 20/10/15.
 */
public class Rogue extends Profession {
    private final String description= " that carries the trappings, leather armor, knives and hooks of a Rogue";
    public Rogue() {
        super(1.25f, 1.f, 0.75f, 1.f, 1.f);
    }
}

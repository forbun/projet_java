package character.professions;

import character.Profession;

/**
 * Created by root on 20/10/15.
 */
public class Mage extends Profession {
    public final String description = " that carries the trappings, robes and hat of a mage";
    public Mage() {
        super(0.75f, 1.25f, 1.f, 0.8f, 1.2f);
    }
}

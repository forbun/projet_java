package character.professions;

import character.Profession;

/**
 * Created by root on 20/10/15.
 */
public class Warrior extends Profession {
    private final String description = " that carries the trappings, armore, short sword and shield of a warrior";
    public Warrior() {
        super(1.f, 0.75f, 1.25f, 1.2f, 0.8f);
    }
}

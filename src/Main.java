import character.Player;
import character.Professions;
import character.Races;
import dungeon_building.Save;
import dungeon_building.exceptions.DoorException;

import java.io.IOException;

/**
 * Created by root on 20/10/15.
 */
public class Main {
    public static void main(String[] args) {
        if(args.length == 0) {
            Game g = new Game();
            g.buildDungeon(0, (int) (System.currentTimeMillis() / 1000));
            g.buildPlayer();

            g.startGame();
        } else {
            Save masave = null;
            try {
                masave = new Save(args[0]);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            // TODO : Make Game() with a save file...
            Game g = new Game(masave);

            g.startGame();
        }
    }
}

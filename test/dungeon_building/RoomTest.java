package dungeon_building;

import dungeon_building.enumerations.Themes;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by root on 27/10/15.
 */
public class RoomTest {
    @Test
    public void print_descr(){
        Dungeon d = new Dungeon(0,0);

        Room myroom = d.go_to_level(0);
        assertTrue(myroom.get_description() instanceof String );
    }
}
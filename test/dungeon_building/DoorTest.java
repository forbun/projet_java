package dungeon_building;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by root on 21/10/15.
 */
public class DoorTest {


    @Test
    public void testOpen() throws Exception {
        Door d = new Door(null,null,false,null,null,null);
        assertTrue(d.open() == true && d.is_closed() == false);
    }

    @Test
    public void testClose() throws Exception {
        Door d = new Door(null,null,false,null,null,null);
        assertTrue(d.open() == true && d.is_closed() == false);
    }

    @Test
    public void testActivate() throws Exception {
        Door d = new Door(null,null,false,new Lock(false,null,null),null,null);
        d.lock.unlock_me();
        assertTrue(d.activate() == true && d.is_closed() == false);
    }

    @Test
    public void testDeactivate() throws Exception {
        Door d = new Door(null,null,false,new Lock(false,null,null),null,null);
        d.lock.unlock_me();
        d.open();
        assertTrue(d.deactivate() && d.is_closed());
    }

    @Test
    public void testGet_joining_rooms() throws Exception {
        Dungeon d = new Dungeon(0,0);
        Place level = new Place(d,0);
        Room[] rooms = new Room[2];
        rooms[0] = new Room(level);
        rooms[1] = new Room(level);
        Door mydoor = new Door(rooms[0],rooms[1],false,null,null,null);

        assertTrue(mydoor.get_joining_rooms()[0] == rooms[0] && mydoor.get_joining_rooms()[1] == rooms[1]);
    }
}
package dungeon_building;

import org.junit.Test;
import static org.junit.Assert.assertTrue;
/**
 * Created by root on 21/10/15.
 */
public class LockTest {

    @Test
    public void testLock_me() throws Exception {
        Lock mylock = new Lock(false,null,null);
        mylock.lock_me();
        assertTrue(mylock.lock_me());

    }

    @Test
    public void testUnlock_me() throws Exception {
        Lock mylock = new Lock(false,null,null);
        assertTrue(mylock.unlock_me());
    }
}
package dungeon_building;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by root on 21/10/15.
 */
public class PlaceTest {

    @Test
    public void test_level_build(){
        Dungeon d = new Dungeon(0,0);
        Place level = new Place(d,0);
        assertTrue(rec_test_level(level.get_room(0),null));
    }

    public boolean rec_test_level(Room cur,Room prev){
        Room[] rooms;
        boolean res;
        if(cur == null){
            return true;
        }
        else{
            rooms = cur.get_entry().get_joining_rooms();
            res = rooms[0] == prev && rooms[1] == cur;
        }

        if(!res){
            return res;
        }
        else{
            return rec_test_level(cur.get_exit().get_joining_rooms()[1],cur);
        }
    }
}
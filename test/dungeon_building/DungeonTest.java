package dungeon_building;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
/**
 * Created by root on 21/10/15.
 */
public class DungeonTest {

    @Test
    public void test_seed(){
        Dungeon D = new Dungeon(0,0);
        assertTrue(D.get_seed() != 0);
        assertTrue(D.build_rng() != 0);
    }

    @Test
    public void test_rng(){
        Dungeon D = new Dungeon(0,0);
        assertTrue(D.build_rng() != D.build_rng());
    }

    @Test
    public void test_go_to_level(){
        Dungeon D = new Dungeon(0,0);
        assertNotEquals(null,D.go_to_level(0));
    }
}
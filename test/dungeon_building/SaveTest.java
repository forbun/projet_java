package dungeon_building;

import character.Player;
import character.Professions;
import character.Races;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by root on 02/11/15.
 */
public class SaveTest {
    @Test
    public void testDump() throws Exception {
        Dungeon d = new Dungeon(0,0);
        Player p = new Player("Player",Races.HUMAIN,Professions.WARRIOR,1);
        p.goToStart(d);

        Save mysave = new Save("test",p);
        mysave.set_name("test");
        mysave.dump();

        Save save2 = new Save("test");
        assertTrue(mysave.get_level() == save2.get_level());
        assertTrue(mysave.get_level_seed() == save2.get_level_seed());
    }
}
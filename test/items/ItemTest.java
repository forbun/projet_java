package items;

import items.type.Boots;
import items.type.ContactWeapon;
import items.type.Food;
import items.type.Ring;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * .
 *
 * @author bunlang
 */
public class ItemTest {
    private ContactWeapon _test1;
    private ContactWeapon _test11;
    private Food _test2;
    private Boots _test3;
    private Ring _test4;

    @Before
    public void setUp() throws Exception {
        _test1 = new ContactWeapon("Ghost dagger", "Aaaah ! A dagger... That's nothing.");
        _test11 = new ContactWeapon("Ghost dagger", "Aaaah ! A dagger... That's nothing.");
        _test2 = new Food("Fish and Chips", "Time to eat !");
        _test3 = new Boots("Dancing boots", "Useful for dance... useless for fight.");
        _test4 = new Ring("One ring", "My precious...");
    }

    @After
    public void tearDown() throws Exception {
        _test1 = null;
        _test2 = null;
        _test3 = null;
        _test4 = null;
    }

    @Test
    public void test_effects() throws Exception {
        assertEquals("No effects when created", _test1.getEffects(), " No effects.\n");

        _test1.addEffect(Affected.LIFE, 20);
        assertEquals("Verify effect", _test1.getEffect(Affected.LIFE), Integer.valueOf(20));

        assertEquals("Removed effect", _test1.removeEffect(Affected.LIFE), Integer.valueOf(20));
    }

    @Test
    public void test_equals() throws Exception {
        // True in this case (same params in constructor)
        assertEquals(_test1, _test11);

        // Obviously not equals (not the same effect)
        _test1.addEffect(Affected.LIFE, 10);
        assertNotEquals(_test1, _test11);

        // Not equals (same kind, but not same power)
        _test11.addEffect(Affected.LIFE, 15);
        assertNotEquals(_test1, _test11);

        // Override old value : equals
        _test1.addEffect(Affected.LIFE, 15);
        assertEquals(_test1, _test11);

        // Equals, even if effects are not set in the same order
        _test1.addEffect(Affected.DEXTERITY, 10);
        _test1.addEffect(Affected.MAX_MANA, -5);
        _test11.addEffect(Affected.MAX_MANA, -5);
        _test11.addEffect(Affected.DEXTERITY, 10);
        assertEquals(_test1, _test11);
    }

    @Test
    public void test_toString() throws Exception {
        _test1.addEffect(Affected.MAX_LIFE, 50);
        String expected = "Ghost dagger :\n" +
                " Aaaah ! A dagger... That's nothing.\n" +
                " Max Life : 50\n";

        assertEquals("Check toString()", _test1.toString(), expected);


    }
}
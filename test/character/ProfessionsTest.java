package character;

import character.professions.*;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * .
 *
 * @author bunlang
 */
public class ProfessionsTest {

    @Test
    public void test_getProfession() throws Exception {
        assertTrue("Check Mage Profession", Professions.MAGE.getProfession() instanceof Mage);
        assertTrue("Check Rogue Profession", Professions.ROGUE.getProfession() instanceof Rogue);
        assertTrue("Check Warrior Profession", Professions.WARRIOR.getProfession() instanceof Warrior);
    }
}
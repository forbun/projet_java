package character;

import items.Affected;
import items.type.Ring;
import dungeon_building.Dungeon;
import dungeon_building.Place;
import dungeon_building.Room;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import static org.junit.Assert.*;

/**
 * .
 *
 * @author bunlang
 */
public class EntityTest {
    private Monster _mnstr;
    private Player _plyr;
    private Ring _ring;

    @Before
    public void setUp() throws Exception {
        Dungeon d = new Dungeon(0,0);
        Room loc = d.go_to_level(0);
        _mnstr = new Monster("Monster", Races.ORC, Professions.WARRIOR,loc);
        _plyr = new Player("Player", Races.ORC, Professions.WARRIOR, 4);
        _ring = new Ring("One Ring", "My precious...");
            _ring.addEffect(Affected.MAX_LIFE, 200);
    }

    @After
    public void tearDown() throws Exception {
        _mnstr = null;
        _plyr = null;
        _ring = null;
    }

    @Test
    public void test_loseLifePoints() {
        int lifePoints = _plyr.getLifePoints();

        _plyr.loseLifePoints(15);

        assertEquals("Should lost 15 Life Pts", _plyr.getLifePoints(), lifePoints - 15);

    }

    @Test
    public void test_useMana() {
        int manaPoints = _plyr.getManaPoints();

        _plyr.useMana(15);

        assertEquals("Should use 15 Mana Pts", _plyr.getManaPoints(), manaPoints - 15);
    }

    @Test
    public void test_haveEnoughMana() {
        int manaPoints = _plyr.getManaPoints();
        assertTrue("Can use all Mana once", _plyr.haveEnoughMana(manaPoints));

        assertTrue("Can't use more Mana than it have", !_plyr.haveEnoughMana(manaPoints + 1));
    }


    @Test
    public void test_haveNoMana() {
        assertTrue("Created Entity should have some Mana", !_plyr.haveNoMana());

        _plyr.useMana(_plyr.getManaPoints());

        assertTrue("Created Entity should now not have Mana", _plyr.haveNoMana());
    }

    @Test
    public void test_getLifePoints() throws Exception {
        assertEquals("Life Points : Monster - should be maximal at the beginning",
                _mnstr.getLifePoints(), _mnstr.getMaxLifePoints());
        assertEquals("Life Points : player - should be maximal at the beginning",
                _plyr.getLifePoints(), _plyr.getMaxLifePoints());
    }

    @Test
    public void test_getManaPoints() throws Exception {
        assertEquals("Mana Points : Monster - should be maximal at the beginning",
                _mnstr.getManaPoints(), _mnstr.getMaxManaPoints());
        assertEquals("Mana Points : player - should be maximal at the beginning",
                _plyr.getManaPoints(), _plyr.getMaxManaPoints());
    }

    @Test
    public void test_getMaxLifePoints() throws Exception {
        assertTrue("Max Life Points : should increase with level",
                _mnstr.getMaxLifePoints() < _plyr.getMaxLifePoints());
    }

    @Test
    public void test_getMaxManaPoints() throws Exception {
        assertTrue("Max Mana Points : should increase with level",
                _mnstr.getMaxManaPoints() < _plyr.getMaxManaPoints());
    }

    @Test
    public void test_getStrength() throws Exception {
        assertTrue("Strength : should increase with level",
                _mnstr.getStrength() < _plyr.getStrength());
    }

    @Test
    public void test_getDexterity() throws Exception {
        assertTrue("Dexterity : should increase with level",
                _mnstr.getDexterity() < _plyr.getDexterity());
    }

    @Test
    public void test_getIntelligence() throws Exception {
        assertTrue("Intelligence : should increase with level",
                _mnstr.getIntelligence() < _plyr.getIntelligence());
    }

    @Test
    public void test_getLevel() throws Exception {
        assertEquals("Level : Monster - should be 1", _mnstr.getLevel(), 1);
        assertEquals("Level : Player - should be 4", _plyr.getLevel(), 4);
    }

    @Test
    public void test_toString() throws Exception {
        String mnstrStr = "Hi! My name is Monster\n" +
                "I'm a Warrior Orc\n" +
                "Level : 1\n" +
                "Life : 159/159 (bonus : 0)\n" +
                "Mana : 53/53 (bonus : 0)\n" +
                "Strength : 146 (bonus : 0)\n" +
                "Dexterity : 116 (bonus : 0)\n" +
                "Intelligence : 49 (bonus : 0)\n" +
                "No equiped.\n";
        String mnstrStr2 = "Hi! My name is Monster\n" +
                "I'm a Warrior Orc\n" +
                "Level : 1\n" +
                "Life : 159/359 (bonus : 200)\n" +
                "Mana : 53/53 (bonus : 0)\n" +
                "Strength : 146 (bonus : 0)\n" +
                "Dexterity : 116 (bonus : 0)\n" +
                "Intelligence : 49 (bonus : 0)\n" +
                "I'm equiped of :\n" +
                "On Finger:\n" +
                "One Ring :\n" +
                " My precious...\n" +
                " Max Life : 200\n";
        String plyrStr = "Hi! My name is Player\n" +
                "I'm a Warrior Orc\n" +
                "Level : 4\n" +
                "Life : 184/184 (bonus : 0)\n" +
                "Mana : 61/61 (bonus : 0)\n" +
                "Strength : 169 (bonus : 0)\n" +
                "Dexterity : 134 (bonus : 0)\n" +
                "Intelligence : 56 (bonus : 0)\n" +
                "No equiped.\n";

        assertEquals(mnstrStr, _mnstr.toString());
        assertEquals(plyrStr, _plyr.toString());

        _mnstr.addItem(_ring);
        _mnstr.equip(_ring);

        assertEquals(mnstrStr2, _mnstr.toString());
    }

    @Test
    public void test_equipment() throws Exception {
        int baseMaxLife = _plyr.getMaxLifePoints();

        _plyr.addItem(_ring);
        _plyr.equip(_ring);

        assertEquals("Max Life should change when the ring is equiped",
                _plyr.getMaxLifePoints(), baseMaxLife + _ring.getEffect(Affected.MAX_LIFE));

        assertEquals("Should be the ring...", _plyr.unequip(BodyPart.FINGER), _ring);
    }

    @Test
    public void test_getBonus() throws Exception {
        _ring.addEffect(Affected.STRENGTH, 200);

        _plyr.addItem(_ring);
        assertEquals("Bonus of 0 for Strength (ring not equiped)",
                _plyr.getBonus(Affected.STRENGTH), 0);

        _plyr.equip(_ring);
        assertEquals("Bonus of 200 for Strength (ring equiped)",
                _plyr.getBonus(Affected.STRENGTH), 200);
        assertEquals("Bonus of 0 for Intelligence (always)",
                _plyr.getBonus(Affected.INTELLIGENCE), 0);
    }

    @Test
    public void test_experience() throws Exception {
        // Level -> xp
        assertEquals("Level -> xp : 1", 0, Entity.levelToXp(1));
        assertEquals("Level -> xp : 2", 1000, Entity.levelToXp(2));
        assertEquals("Level -> xp : 3", 2150, Entity.levelToXp(3));
        assertEquals("Level -> xp : 4", 3473, Entity.levelToXp(4));
        assertEquals("Level -> xp : 5", 4994, Entity.levelToXp(5));
        assertEquals("Level -> xp : 6", 6743, Entity.levelToXp(6));
        assertEquals("Level -> xp : 7", 8754, Entity.levelToXp(7));
        assertEquals("Level -> xp : 8", 11067, Entity.levelToXp(8));
        assertEquals("Level -> xp : 9", 13727, Entity.levelToXp(9));
        assertEquals("Level -> xp : 10", 16786, Entity.levelToXp(10));

        // xp -> Level
        assertEquals("Xp ->level : 0     -> lvl  1+", 1, Entity.xpToLevel(0));
        assertEquals("Xp ->level : 999   -> lvl  1-", 1, Entity.xpToLevel(999));
        assertEquals("Xp ->level : 1000  -> lvl  2+", 2, Entity.xpToLevel(1000));
        assertEquals("Xp ->level : 2149  -> lvl  2-", 2, Entity.xpToLevel(2149));
        assertEquals("Xp ->level : 2150  -> lvl  3+", 3, Entity.xpToLevel(2150));
        assertEquals("Xp ->level : 3472  -> lvl  3-", 3, Entity.xpToLevel(3472));
        assertEquals("Xp ->level : 3473  -> lvl  4+", 4, Entity.xpToLevel(3473));
        assertEquals("Xp ->level : 4993  -> lvl  4-", 4, Entity.xpToLevel(4993));
        assertEquals("Xp ->level : 4994  -> lvl  5+", 5, Entity.xpToLevel(4994));
        assertEquals("Xp ->level : 6742  -> lvl  5-", 5, Entity.xpToLevel(6742));
        assertEquals("Xp ->level : 6743  -> lvl  6+", 6, Entity.xpToLevel(6743));
        assertEquals("Xp ->level : 8753  -> lvl  6-", 6, Entity.xpToLevel(8753));
        assertEquals("Xp ->level : 8754  -> lvl  7+", 7, Entity.xpToLevel(8754));
        assertEquals("Xp ->level : 11066 -> lvl  7-", 7, Entity.xpToLevel(11066));
        assertEquals("Xp ->level : 11067 -> lvl  8+", 8, Entity.xpToLevel(11067));
        assertEquals("Xp ->level : 13726 -> lvl  8-", 8, Entity.xpToLevel(13726));
        assertEquals("Xp ->level : 13727 -> lvl  9+", 9, Entity.xpToLevel(13727));
        assertEquals("Xp ->level : 16785 -> lvl  9-", 9, Entity.xpToLevel(16785));
        assertEquals("Xp ->level : 16786 -> lvl 10+",10, Entity.xpToLevel(16786));
    }

    @Test
    public void test_experienceGained() {
        assertEquals(_mnstr.getLevel(), 1);
        _mnstr.addXP(1500);
        assertEquals(_mnstr.getLevel(), 2);
    }
}

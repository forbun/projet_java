package character;

import character.races.*;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * .
 *
 * @author bunlang
 */
public class RacesTest {

    @Test
    public void test_getRace() throws Exception {
        assertTrue("Check Orc Race", Races.ORC.getRace() instanceof Orc);

    }
}